package com.github.akitory4.ellaapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView


class StartActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //свой код
        setContentView(R.layout.start_activity)

        val button = this.findViewById<Button>(R.id.changeTextButton)
        val ellaTextView = this.findViewById<TextView>(R.id.ellaTextView)

        button.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                val intent = Intent(view.context, TotoroActivity::class.java)
                startActivity(intent)
            }
        })


    }
}